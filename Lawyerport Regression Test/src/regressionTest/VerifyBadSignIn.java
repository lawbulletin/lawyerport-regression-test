package regressionTest;

import lp_dominion.AbstractSprint;
import lp_dominion.PFCaller;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import pagefactorylibrary.HomePageFactory;
import pagefactorylibrary.LoginPageFactory;
import seleniumWebDriver.SeleniumDriver;

public class VerifyBadSignIn extends AbstractSprint{

	LoginPageFactory log = PFCaller.loginFactory();
	HomePageFactory home = PFCaller.homeFactory();

	@Test
	public void verifyValidUserInvalidPassword(){
		
		System.out.println("Verify Valid User, Invalid Password Credentials Error Message");
		log.getUsername().clear();
		log.getUsername().sendKeys("alptester3001@lawyerport.com");
		forceWait(2);
		log.getPassword().sendKeys("xyzq");
		forceWait(2);
		log.getClient().sendKeys("client");
		forceWait(2);
		log.getSubmitButton().click();
		
		e = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains (@id, 'msg')]"));
		
		String msg = e.getText().toString();
		
			if(!msg.contains("combination")){
				System.err.println("Fail");
			}
			else{System.out.println("Expected message: " + msg + "   - Pass");}
		
		
		System.out.println("------------------");
		
		//SeleniumDriver.getDriver().close();
		//SeleniumDriver.getDriver().quit();
	}
	
	@Test
	public void verifyInvalidUserSomePassword(){
		home.getsignInButton().click();
		System.out.println("Verify Invalid User Credentials Error Message");
		log.getUsername().clear();
		log.getUsername().sendKeys("HijuanEscotel@SqaCoedSquashTeam.net");
		log.getPassword().sendKeys("test");
		log.getClient().sendKeys("client");
		log.getSubmitButton().click();
		
		e = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains (@id, 'msg')]"));
		
		String msg = e.getText().toString();
		
			if(!msg.contains("try again")){
				System.err.println("Fail");
			}
			else{System.out.println("Expected message: " + msg + "   - Pass");}
		
		System.out.println("------------------");
	}
	
}


