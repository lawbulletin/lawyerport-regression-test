package regressionTest;

import org.junit.Test;
import org.openqa.selenium.Keys;

import lp_dominion.AbstractSprint;
import lp_dominion.PFCaller;
import pagefactorylibrary.CourtDocketResultFactory;
import pagefactorylibrary.CDPageFactory;
import pagefactorylibrary.LPMainPageFactory;

public class VerifyCourtDockets extends AbstractSprint{
	
	CDPageFactory cd = PFCaller.cdFactory();
	LPMainPageFactory main = PFCaller.lpMainFactory();
	CourtDocketResultFactory cdResults = PFCaller.cdResults();

	@Test
	public void docketSearchUsingCaseNum() throws Exception{
		//logIn();
		System.out.println("Docket Search Test");
		main.getLegalResearchLink().click();
		forceWait(1);
		cd.getDocketRestSearchButton().click();
//		cd.getJudgeList().click();
//		cd.getJudgeList().sendKeys(Keys.BACK_SPACE);
		forceWait(2);
		cd.getDocketCaseNum().click();
		cd.getDocketCaseNum().clear();
		cd.getDocketCaseNum().sendKeys("2009D-79039");
		cd.getDocketSearchButton().click();		
//		cdResults.getCDFirstResult().click();
		goToURL("https://www.lawyerport.com/research#/court-dockets/view-results/&caseNumber=2009D-79039/&courtDocketId=7122654");
		forceWait(3);
//		logIn();
		String totalRecords = cdResults.getTotalRecords().getText().toString();
		
		if (totalRecords.contains("278")){
			
			System.out.println("Search results match");
		}
		
		forceWait(3);
		
		cdResults.getFirstRecordCaseNumberSearch().click();
		forceWait(2);
		cdResults.getOkButton().click();	
		
		//tearDown();
	}
	
	@Test
	public void docketSearchUsingJudge() throws Exception{
//		logIn();
		main.getLegalResearchLink().click();
		System.out.println("Test for DOcket Search using Judges");
		forceWait(2);
		cd.getDocketRestSearchButton().click();
		forceWait(1);
		cd.getDocketJudge().click();
		forceWait(2);
		cd.getDocketJudge().sendKeys("HOG");
		forceWait(2);
		cd.getDocketJudge().sendKeys(Keys.DOWN);
		cd.getDocketJudge().sendKeys(Keys.ENTER);
		forceWait(4);
		cd.getDocketSearchButton().click();
		forceWait(3);
		
		goToURL("https://www.lawyerport.com/research#/court-dockets/view-results/&judge=492/&courtDocketId=152723");
		forceWait(3);
		
		cdResults.getFirstRecordJudgeSearch().click();
		forceWait(2);
		cdResults.getOkButton().click();
		forceWait(2);
		main.getLegalResearchLink().click();
	}
	
	@Test
	public void docketSearchUsingFirm() throws Exception{
		
		main.getLegalResearchLink().click();
		System.out.println("Test for Docket Search using Lawyer/Firm ");
		forceWait(3);
		cd.getDocketRestSearchButton().click();
		forceWait(1);
		cd.getDocketLawyerFirm().click();
		
		forceWait(2);
		cd.getDocketLawyerFirm().sendKeys("jenner");
		forceWait(3);
		cd.getDocketLawyerFirm().sendKeys(Keys.DOWN);
		cd.getDocketLawyerFirm().sendKeys(Keys.ENTER);
		forceWait(2);
		cd.getDocketSearchButton().click();
		 forceWait(3);
		 
		 goToURL("https://www.lawyerport.com/research#/court-dockets/view-results/&lawyerFirm=22653/&courtDocketId=8004476");
		 
		 forceWait(3);
		 
		 cdResults.getFirstRecordLawyerSearch().click();
		 forceWait(3);
		 cdResults.getOkButton().click();
	}
	
	@Test
	public void docketSearchUsingPlaintiffDefendant() throws Exception{
		//logIn();
		forceWait(2);
		main.getLegalResearchLink().click();
		cd.getDocketRestSearchButton().click();
		System.out.println("Test for Docket Search Using Plaintiff/Defendant");
		forceWait(3);
		
		cd.getDocketPlaintiffDefendant().click();
		
		forceWait(2);
		
		
		cd.getDocketPlaintiffDefendant().sendKeys("boeing");
		forceWait(2);
		
		cd.getDocketSearchButton().click();
		
		forceWait(3);
		
		goToURL("https://www.lawyerport.com/research#/court-dockets/view-results/&plaindef=boeing/&courtDocketId=8875445");
		
		forceWait(3);
		
		cdResults.getFirstRecordPlaintiffDefendantSearch().click();
		
		forceWait(2);
		
		cdResults.getOkButton().click();
	}
	
	@Test
	public void TestForNavigationButtons(){
//		logIn();
		main.getLegalResearchLink().click();
		cd.getDocketRestSearchButton().click();
		System.out.println("Test for Docket Search Header Navigation Buttons using Plaintiff/Defendant");
		forceWait(3);
		
		cd.getDocketPlaintiffDefendant().click();
		
		forceWait(2);
		
		cd.getDocketPlaintiffDefendant().clear();
		forceWait(2);
		cd.getDocketPlaintiffDefendant().sendKeys("jon");
		
		forceWait(1);
		
		cd.getDocketSearchButton().click();
		
		forceWait(2);
		
		String showing = cdResults.getResultCount().getText().toString();
		
		if(showing.contains("50")){
			
			System.out.println("First Page");
		} 
		forceWait(2);
		 cdResults.getNextButtonPlaintiffSearchHeader().click();
		 
		 forceWait(1);
		 
		 String showing2 = cdResults.getResultCount().getText().toString();
		 
		 if(showing2.contains("51")){
			 
			 System.out.println("Second Page");
			 System.out.println("Next button in header works");
		 } else {
			 
			 System.err.println("Navigation using Next button doesn't work. ");
		 }
		 
		 cdResults.getPreviousButtonPlaintiffSearchHeader().click();
		 
		 forceWait(1);
		 
		 String showing3 = cdResults.getResultCount().getText().toString();
		 
		 if(showing3.contains("50")){
			 
			 System.out.println("Previous Button in header works");
		 } else {
			 
			 System.err.println("Navigation through previous button failed");
		 }
		 
		 cdResults.getLastButtonPlaintiffSearchHeader().click();
		 
		 if(cdResults.getPreviousButtonPlaintiffSearchHeader().isDisplayed() && cdResults.getFirstButtonPlaintiffSearchHeader().isDisplayed()){
			 
			 System.out.println("Last Button in Header works");
		 } else {
			 
			 System.err.println("Navigation using Last button failed");
		 }
		 
		 forceWait(1);
		 
		 cdResults.getFirstButtonPlaintiffSearchHeader().click();
		 
		 String showing4 = cdResults.getResultCount().getText().toString();
		 
		 if(showing4.contains("50")){
			 
			 System.out.println("First button in Header works");
		 } else {
			 
			 System.err.println("Navigation to first page using First button failed");
		 }
		 
		 System.out.println("------------------------------------");
		 
		 System.out.println("Test for Docket Search Footer Navigation Buttons using Plaintiff/Defendant");
		 
		 main.getLegalResearchLink().click();
		 
		 forceWait(2);
		 
		 cd.getDocketRestSearchButton().click();
		 
		 forceWait(1);
		 
		 cd.getDocketPlaintiffDefendant().sendKeys("Jon");
		 
		 forceWait(1);
		 
		 cd.getDocketSearchButton().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getNextButtonPlaintiffSearchFooter().isDisplayed() && cdResults.getNextButtonPlaintiffSearchFooter().isDisplayed()){
			 
			 System.out.println("First Page");
			 
		 }
		 
		 cdResults.getNextButtonPlaintiffSearchFooter().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getResultCount().getText().toString().contains("51")){
			 
			 System.out.println("Second Page.. Test for navigation using footer next button passed..");
		 } else {
			 
			 System.out.println("Test for navigation using footer next button failed..");
		 }
		 
		 cdResults.getPreviousButtonPlaintiffSearchFooter().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getResultCount().getText().toString().contains("50")){
			 
			 System.out.println("Test for navigation to previous page using Footer Previous button passed..");
		 } else {
			 
			 System.out.println("test for Footer Previous button failed.. ");
		 }
		 
		 cdResults.getLastButtonPlaintiffSearchFooter().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getPreviousButtonPlaintiffSearchFooter().isDisplayed() && cdResults.getFirstButtonPlaintiffSearchFooter().isDisplayed()){
			 
			 System.out.println("Test for navigation to last page using Footer Last button passed..");
		 } else {
			 
			 System.out.println("Test for Footer Last Button failed");
		 }
		  cdResults.getFirstButtonPlaintiffSearchFooter().click();
		  
		  forceWait(2);
		  
		  if(cdResults.getResultCount().getText().toString().contains("50")){
			  
			  System.out.println("Test fo navigation to First page using Footer First button passed.. ");
		  } else {
			  
			  System.out.println("Navigation using Footer First Button failed..");
		  }
		  
		 System.out.println("-----------------------------------------------------------------------");
		 
		 
		 System.out.println("Testing Navigation buttons for docket search using Case Number");
		 
		 main.getLegalResearchLink().click();
		 
		 forceWait(2);
		 
		 cd.getDocketRestSearchButton().click();
		 
		 cd.getDocketCaseNum().sendKeys("2005L-3541");
		 
		 forceWait(2);
		 
		 goToURL("https://www.lawyerport.com/research#/court-dockets/view-results/&caseNumber=2005L-3541/&courtDocketId=6005446");
		 
		 forceWait(3);
		 
		 cdResults.getResultPage2().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getCaseSearchCount().getText().contains("51")){
			 
			 System.out.println("Navigation test page 2 passed");
		 } else{
			 
			 System.out.println("Page 2 button failed.");
		 }
		 
		 cdResults.getResultPage3().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getCaseSearchCount().getText().contains("150")){
			 
			 System.out.println("Navigation test page 3 passed ");
		 } else {
			 
			 System.out.println("page 3 button failed.");
		 }
		 
		 cdResults.getResultPage4().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getCaseSearchCount().getText().contains("200")){
			 
			 System.out.println("Navigation test page 4 passed");
		 } else {
			 
			 System.out.println("Page 4 button failed");
		 }
		 
		 cdResults.getResultPage5().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getCaseSearchCount().getText().contains("250")){
			 
			 System.out.println("Navigation test page 5 passed");
		 } else {
			 
			 System.out.println("Page 5 button failed");
		 }
		 
		 cdResults.getFirstButtonCaseSearch().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getCaseSearchCount().getText().contains("50")){
			 
			 System.out.println("Navigation test previous page passed");
		 } else {
			 
			 System.out.println("Previous button failed");
		 }
		 
		 cdResults.getNextButtonCaseSearch().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getCaseSearchCount().getText().contains("100")){
			 
			 System.out.println("Navigation test Next button passed");
		 } else {
			 
			 System.out.println("Next button failed");
		 }
		 
		 cdResults.getLastButtonCaseSearch().click();
		 
		 forceWait(2);
		 
		 System.out.println("Navigation test Last button passed ");
		 
		 System.out.println("-----------------------------------------------------------------------------------");
		 System.out.println("Test for Navigation button in Footer using Case Search");
		 
		 cdResults.getCaseSearchFirstButtonFooter().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getCaseSearchCount().getText().contains("50")){
			 
			 System.out.println("Test for First page button in Footer passed.. ");
		 } else {
			 
			 System.out.print("Navigation using First button failed . ");
		 }
		 
		 cdResults.getCaseSearchNextButtonFooter().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getCaseSearchCount().getText().contains("100")){
			 
			 System.out.println("Test for Next Button in footer passed.. ");
		 } else {
			 
			 System.out.println("Navigation using Next button failed.");
		 }
		 
		 cdResults.getCaseSearchPreviousButtonFooter().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getCaseSearchCount().getText().contains("50")){
			 
			 System.out.println("Test for Previous button in footer passed.. ");
		 } else {
			 
			 System.out.println("Navigation using Previous button failed.");
		 }
		 
		 cdResults.getPage5FooterCaseSearch().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getCaseSearchCount().getText().contains("250")){
			 
			 System.out.println("Test for Page 5 button in footer passed..");
		 } else {
			 
			 System.out.println("Navigation using Page 5 button failed.");
		 }
		 
		 cdResults.getPage4FooterCaseSearch().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getCaseSearchCount().getText().contains("200")){
			 
			 System.out.println("Test for Page 4 button in footer passed.. ");
		 } else {
			 
			 System.out.println("Navigation using Page 4 button failed.");
		 }
		 
		 cdResults.getPage3FooterCaseSearch().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getCaseSearchCount().getText().contains("150")){
			 
			 System.out.println("Test for Page 3 button in footer passed.. ");
		 } else {
			 
			 System.out.println("Navigation using Page 3 button failed.");
		 }
		 
		 cdResults.getPage2FooterCaseSearch().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getCaseSearchCount().getText().contains("100")){
			 
			 System.out.println("Test for Page 2 button in footer passed..");
		 } else {
			 
			 System.out.println("Navigation using Page 2 button failed.");
		 }
		 
		 cdResults.getPage1FooterCaseSearch().click();
		 
		 forceWait(2);
		 
		 if(cdResults.getCaseSearchCount().getText().contains("50")){
			 
			 System.out.println("Test for Page 1 button in footer passed.. ");
		 } else {
			 
			 System.out.println("Navigation using Page 1 button failed.");
		 }
		 
		 cdResults.getCaseSearchLastButtonFooter().click();
		 
		 forceWait(2);
		 
		 System.out.println("Test for Last button in Footer passed.. ");
		 
	}
	
}
