package regressionTest;

import org.junit.Test;

import lp_dominion.AbstractSprint;
import lp_dominion.PFCaller;
import pagefactorylibrary.CourtRulePageFactory;
import pagefactorylibrary.LPMainPageFactory;
import pagefactorylibrary.LegalResearchPageFactory;

public class VerifyCourtRules extends AbstractSprint{
	
	CourtRulePageFactory courtRule = PFCaller.crFactory();
	LPMainPageFactory main = PFCaller.lpMainFactory();
	LegalResearchPageFactory lr = PFCaller.lrFactory();
	
	
	@Test
	public void navigateToCourtRules(){
		
		System.out.println("Test for navigating to Court Rules.");
		//logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getCourtRules().click();
		
		if(getText("COURT RULES")){
			
			System.out.println("Test for navigation passed..");
		} else {
			
			System.err.println("Test for navigation failed..");
		}
		
	}
	
	@Test
	public void legalResearchNavigationBar(){
		
		System.out.println("Test for Legal Research Navigation Bar");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getCourtRules().click();
		
		if(courtRule.getLegalReasearchNavigationBar().isDisplayed()){
			
			System.out.println("Legal Research Navigation Bar is available , Test passed... ");
		} else {
			
			System.err.println("Legal Research Navigation Bar not found, Test failed..");
		}
		
	}
	
	@Test
	public void courtRuleSearchBackGroundColor(){
		
		String color;
		
		System.out.println("Test for background color of Search Column");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getCourtRules().click();
		forceWait(1);
		
		color = courtRule.getSearchColumn().getCssValue("background-color");
		
		System.out.println(color);
		
		if(courtRule.getSearchColumn().getCssValue("background-color").contains("219")){
			
			System.out.println("BackGround color of Search column matches, Test passed...");
		} else {
			
			System.err.println("BackGround-color of Search column doesn't match , Test failed.");
		}
	}
	
	@Test
	public void courtRuleCenterBackGroundColor(){
		
		String color;
		
		System.out.println("Test for background color of Center Column");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getCourtRules().click();
		forceWait(1);
		
		color = courtRule.getCenterColumn().getCssValue("background-color");
		
		System.out.println(color);
		
		if(courtRule.getCenterColumn().getCssValue("background-color").contains("255")){
			
			System.out.println("BackGround color for Center column matches, Test passed..");
		} else {
			System.err.println("BackGround color doesn't match, Test failed.");
		}
		
		//tearDown();
		
	}
	
	@Test
	public void verifyPageElements(){
		
		Boolean keyword;
		Boolean jurisdiction;
		Boolean ruleNumber;
		Boolean rulesTitle;
		Boolean searchButton;
		Boolean resetButton;
		Boolean headingText;
		Boolean subHeading;
		
		System.out.println("Test for Page elements");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getCourtRules().click();
		forceWait(1);
		
		keyword = courtRule.getKeywords().isDisplayed();
		jurisdiction = courtRule.selectJurisdiction().isEmpty();
		ruleNumber = courtRule.getRuleNumber().isDisplayed();
		rulesTitle = courtRule.getRuleTitle().isDisplayed();
		searchButton = courtRule.getSearchButton().isDisplayed();
		resetButton = courtRule.getRestButton().isDisplayed();
		headingText = courtRule.getHeadingText().isDisplayed();
		subHeading = courtRule.getSubHeading().isDisplayed();
		
		if(keyword && !jurisdiction && ruleNumber && rulesTitle && searchButton && resetButton && headingText && subHeading){
			
			System.out.println("Test for Page Elements passed..");
		} else {
			
			System.err.println("Test for Page Elements failed.");
		}
		
	}
	
	@Test
	public void federalRulesLinks(){
		
		Boolean supremeCourt;
		Boolean rulesOfEvidence;
		Boolean rulesOfCivilProcedure;
		Boolean rulesOfCriminalProcedure;
		Boolean rulesOfAppellateProcedure;
		Boolean rulesOfUS7thCircuit;
		Boolean federalBankruptcyRules;
		Boolean rulesOfUSDistrictNorthern;
		Boolean rulesOfUSBankruptcyNorthern;
		Boolean rulesOfUSDistrictSouthern;
		Boolean rulesOfUSBankruptcySouthern;
		Boolean rulesOfUSDistrictCentral;
		Boolean rulesOfUSBankruptcyCentral;
		
		System.out.println("Test for Federal Rules Links");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getCourtRules().click();
		forceWait(1);
		
		supremeCourt = courtRule.getRulesOfUnitedStatesSupremeCourt().getAttribute("href").contains("83257");
		rulesOfEvidence = courtRule.getFederalRulesOFEvidence().getAttribute("href").contains("83315");
		rulesOfCivilProcedure = courtRule.getFederalRulesOfCivilProcedure().getAttribute("href").contains("83395");
		rulesOfCriminalProcedure = courtRule.getFederalRulesOfCriminalProcedure().getAttribute("href").contains("83503");
		rulesOfAppellateProcedure = courtRule.getFederalRulesOfAppellateProcedure().getAttribute("href").contains("83588");
		rulesOfUS7thCircuit = courtRule.getRulesOf7thCircuit().getAttribute("href").contains("83651");
		federalBankruptcyRules = courtRule.getFederalBankruptcyRules().getAttribute("href").contains("83691");
		rulesOfUSDistrictNorthern = courtRule.getRulesOfUSDistrictCourtNorthern().getAttribute("href").contains("83918");
		rulesOfUSBankruptcyNorthern = courtRule.getRulesOfBankruptcyCourtNorthern().getAttribute("href").contains("84109");
		rulesOfUSDistrictSouthern = courtRule.getRulesOfUSDistrictCourtSouthern().getAttribute("href").contains("84228");
		rulesOfUSBankruptcySouthern = courtRule.getRulesOfUSBankruptcyCourtSouthern().getAttribute("href").contains("84271");
		rulesOfUSDistrictCentral = courtRule.getRulesOfUSDistrictCourtCentral().getAttribute("href").contains("84365");
		rulesOfUSBankruptcyCentral = courtRule.getRulesOFUSBankruptcyCourtCentral().getAttribute("href").contains("84450");
		
		
		if(supremeCourt && rulesOfEvidence && rulesOfCivilProcedure && rulesOfCriminalProcedure && rulesOfAppellateProcedure && rulesOfUS7thCircuit &&  federalBankruptcyRules && rulesOfUSDistrictNorthern  && rulesOfUSBankruptcyNorthern && rulesOfUSDistrictSouthern && rulesOfUSBankruptcySouthern && rulesOfUSDistrictCentral && rulesOfUSBankruptcyCentral){
			
			System.out.println("Test for Federal Rules Links passed...");
		} else {
			
			System.err.println("Test for Federal Rules failed.");
		}
			
	}
	
//	@Test
//	public void illinoisCourtRules(){
//		
//		
//	}
	
}
