package regressionTest;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import lp_dominion.AbstractSprint;
import lp_dominion.PFCaller;
import pagefactorylibrary.JVRPageFactory;
import pagefactorylibrary.JVRResultFactory;
import pagefactorylibrary.LPMainPageFactory;
import pagefactorylibrary.LegalResearchPageFactory;
import seleniumWebDriver.SeleniumDriver;

public class VerifyJuryVerdicts extends AbstractSprint{
	
	JVRPageFactory jvr = PFCaller.jvrFactory();
	LPMainPageFactory main = PFCaller.lpMainFactory();
	LegalResearchPageFactory lr = PFCaller.lrFactory();
	JVRResultFactory result = PFCaller.jvrResult();
	
	@Test
	public void verdictCaseNumberSearch() throws Exception{
		
		System.out.println("Jury Verdict test for search using Case Number");
//		logIn();
		System.out.println("Navigating to Legal Research");
		forceWait(1);
		main.getLegalResearchLink().click();
		forceWait(1);
		System.out.println("Navigating to Verdicts & Settlements");
		lr.getJVS().click();
		forceWait(2);
		jvr.getCaseNumberSearch().click();
		jvr.getCaseNumberSearch().sendKeys("79L-3014");
		jvr.getJVRSearchButton().click();
		System.out.println("Selecting first result");
		result.getFirstResultCheckBox().click();
		result.getSelected().click();
		System.out.println("Verifying case number");
		if(result.getCaseCheckText().getText().toString().contains("79L-3014")){
			System.out.println("Case Number Matches");
		} else {
			
			System.err.println("Case Number doesnt match... Test Failed");
		}
		main.getHomeIcon().click();
		forceWait(2);
		
	}
	
	@Test
	public void verdictKeywordSearch1() throws Exception{
		
		//logIn();
		System.out.println("--------------------------------------------------------------");
		System.out.println("Jury Verdict test #1 for search using a keyword");
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		
		jvr.getReset().click();
		forceWait(2);
		
		jvr.getKeyword().sendKeys("(burglary AND home) NOT (house OR residence)");
		forceWait(1);
		jvr.getJVRSearchButton().click();
		forceWait(3);
		
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		
		if(result.getTextSelectedCases().isDisplayed()){
			
			System.out.println("Test Passed..");
		} else {
			
			System.err.println("Test Failed.");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
		
	}
	
	@Test
	public void verdictKeywordSearch2() throws Exception{
		System.out.println("--------------------------------------------------------------");
		System.out.println("Jury Verdict test #2 for search using a keyword");
		
		forceWait(2);
		
		
		
		main.getLegalResearchLink().click();
		forceWait(2);
		
		lr.getJVS().click();
		forceWait(2);
		
		jvr.getReset().click();
		forceWait(2);
		
		jvr.getKeyword().sendKeys("\"Lloyd's of London\"");
		forceWait(1);
		
		jvr.getJVRSearchButton().click();
		forceWait(3);
		
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(2);
		
		if(result.getTextCheckForFirstResultKeyword2().getText().contains("Lloyd's of London") && result.getTextCheckForFirstResultKeyword2().getCssValue("background-color").contains("255")){
			
			System.out.println("Test #2 for search using keyword passed..");
		} else {
			
			System.err.println("Test #2 for search using keyword failed.");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictKeywordSearch3() throws Exception{
		System.out.println("--------------------------------------------------------------");
		System.out.println("Jury Verdict test #3 for search using a keyword");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		
		jvr.getKeyword().sendKeys("\"bus\"");
		jvr.getJVRSearchButton().click();
		forceWait(3);
		if (result.getContinueButton().isDisplayed()){
			
			result.getContinueButton().click();
		}
		forceWait(1);
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(1);
		
		if(result.getTextCheckForFirstResultKeyword3().getText().contains("bus") && result.getTextCheckForFirstResultKeyword3().getCssValue("background-color").contains("255")){
			
			System.out.println("Test #3 for search using keyword passed..");
		} else {
			
			System.err.println("Test #3 for search using keyword failed.");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictKeywordSearch4(){
		System.out.println("--------------------------------------------------------------");
		System.out.println("Jury Verdict test #4 for search using a keyword");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		
		jvr.getKeyword().sendKeys("\"taxi\"");
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		if (result.getContinueButton().isDisplayed()){
			
			result.getContinueButton().click();
		}
		
		forceWait(1);
		
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(1);
		
		if(result.getTextCheckForFirstResultKeyword4().getText().contains("taxi") && result.getTextCheckForFirstResultKeyword4().getCssValue("background-color").contains("255")){
			
			System.out.println("Test #4 for search using keyword passed..");
		} else {
			
			System.err.println("Test #4 for search using keyword failed.");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictKeywordSearch5() throws Exception{
		System.out.println("--------------------------------------------------------------");
		System.out.println("Jury Verdict test #5 for search using keyword");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		
		jvr.getKeyword().sendKeys("\"the stop sign\"");
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		if (result.getContinueButton().isDisplayed()){
			
			result.getContinueButton().click();
		}
		
		forceWait(2);
		
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(1);
		
		if(result.getTextCheckForFirstResultKeyword5().getText().contains("the stop sign") && result.getTextCheckForFirstResultKeyword5().getCssValue("background-color").contains("255")){
			
			System.out.println("Test #5 for search using keyword passed..");
		} else {
			
			System.err.println("Test #5 for search using keyword failed.");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictKeywordSearch6() throws Exception{
		System.out.println("--------------------------------------------------------------");
		System.out.println("Jury Verdict test #6 for search using keyword");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		
		
		jvr.getKeyword().sendKeys("bus AND \"stop sign\"");
		
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(2);
		
		if(result.getTextCheck1ForFirstResultKeyword6().getText().contains("stop sign") && result.getTextCheck2ForFirstResultKeyword6().getText().contains("bus")){
			
			System.out.println("Test #6 for search using keyword passed...");
		} else {
			
			System.err.println("Test #6 Failed.....................");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictKeywordSearch7() throws Exception{
		System.out.println("--------------------------------------------------------------");
		System.out.println("Jury Verdict test #7 for search using a keyword");
		
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		
		jvr.getReset().click();
		forceWait(2);
		
		jvr.getKeyword().sendKeys("criminal AND police");
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(1);
		
		if(result.getTextCheck1ForFirstResultKeyword7().getText().contains("criminal") && result.getTextCheck2ForFirstResultKeyword7().getText().contains("Police")){
			
			System.out.println("Test #7 for search using keyword passed...");
		} else {
			
			System.err.println("Test #7 for search using keyword failed.");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictKeywordSearch8() throws Exception{
		System.out.println("--------------------------------------------------------------");
//		System.out.println("JVR Test Cases for Keyword");
//		logIn();
		System.out.println("Jury Verdict test #8 for search using a keyword");
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		
		jvr.getReset().click();
		forceWait(2);
		
		jvr.getKeyword().sendKeys("Marc Soriano");
		forceWait(1);
		
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(1);
		
		if(result.getTextCheckForFirstResultKeyword8().getText().contains("Marc") && result.getTextCheckForFirstResultKeyword8().getCssValue("background-color").contains("255")){
			
			System.out.println("Test #8 for Keyword Search passed.. ");
		} else {
			
			System.err.println("Test #8 for Keyword Search Failed.");
		}
		
		main.getHomeIcon().click();
		forceWait(3);
			
	}
	
	@Test
	public void verdictKeywordSearch9() throws Exception{
		System.out.println("--------------------------------------------------------------");
		System.out.println("Jury Verdict test #9 for search using a keyword");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(2);
		
		lr.getJVS().click();
		forceWait(1);
		
		jvr.getReset().click();
		forceWait(2);
		
		jvr.getKeyword().sendKeys("Gary Schaer");
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		result.getSelectAllResults().click();
		result.getSelected().click();
		forceWait(2);
		
		if(result.getTextCheckSelectAllForKeyword9().getText().contains("Gary") && result.getTextCheckSelectAllForKeyword9().getCssValue("background-color").contains("255")){
			
			System.out.println("Test #9 for search using keyword and select All button passed..");
		} else {
			
			System.err.println("Test #9 for search using keyowrd and Select All failed.");
		}
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictDateRangeSearch1(){
		System.out.println("--------------------------------------------------------------");
		System.out.println("Test #1 for search using date range");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(2);
		
		lr.getJVS().click();
		forceWait(1);
		jvr.getReset().click();
		
		jvr.getStartDate().sendKeys("07/01/1990");
		forceWait(1);
		jvr.getEndDate().clear();
		jvr.getEndDate().sendKeys("01/01/1991");
		
		jvr.getJVRSearchButton().click();
		forceWait(1);
		
		if (result.getContinueButton().isDisplayed()){
			
			result.getContinueButton().click();
		}
		forceWait(1);
		
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(2);
		
		if(result.getTextCheckForDateRange().getText().contains("12/21/1990")){
			
			System.out.println("Test #1 for search using Date Range passed..");
		} else {
			
			System.err.println("Test #1 for search using Date Range failed.");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictDateRangeSearch2(){
		System.out.println("--------------------------------------------------------------");
		System.out.println("Test #2 for search using date range");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(2);
		
		lr.getJVS().click();
		forceWait(1);
		jvr.getReset().click();
		forceWait(2);
		
		
		jvr.getEndDate().sendKeys("01/01/1989");
		forceWait(1);
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		result.getContinueButton().click();
		forceWait(1);
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(2);
		
		if(result.getTextCheckForDateRange().getText().contains("12/23/1988")){
			
			System.out.println("Test #2 for search using Date range passed...");
		} else {
			
			System.err.println("Test #2 for search using Date Range failed.");
		}
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictPublicationTypeSearch1(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #1 for search using Publication type");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		
		jvr.getKeyword().sendKeys("taxi");
		forceWait(1);
		jvr.getPublicationType(1).click();
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		
		if(result.getTextCheckForPublication1().getText().contains("taxi") && result.getTextCheckForPublication1().getCssValue("background-value").contains("255")){
			
			System.out.println("Test #1 for search using Publication Type passed..");
		} else {
			
			System.err.println("Test #1 for search using Publication type failed.");
		}
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictPublicationTypeSearch2(){
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #2 for search using Publication type");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		
		jvr.getKeyword().sendKeys("bus");
		forceWait(1);
		jvr.getPublicationType(2).click();
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		
		if(result.getTextCheck1ForPublication2().getText().contains("bus") && result.getTextCheck2ForPublication2().getText().contains("bus") && result.gettextCheck3ForPublication2().getText().contains("bus")){
			
			System.out.println("Test #2 for search using Publication Type passed..");
		} else {
			
			System.err.println("Test #2 for search using Publication type failed.");
		}
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictPublicationTypeSearch3(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #3 for search using Publication type");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		
		jvr.getKeyword().sendKeys("tall");
		forceWait(1);
		jvr.getPublicationType(3).click();
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		
		if(result.getTextCheckForPublication3().getText().contains("tall") && result.getTextCheckForPublication3().getCssValue("background-value").contains("255")){
			
			System.out.println("Test #3 for search using Publication Type passed..");
		} else {
			
			System.err.println("Test #3 for search using Publication type failed.");
		}
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictPublicationTypeSearch4(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #4 for search using Publication type");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		
		jvr.getKeyword().sendKeys("French");
		forceWait(1);
		jvr.getPublicationType(4).click();
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		if (result.getContinueButton().isDisplayed()){
			
			result.getContinueButton().click();
		}
		forceWait(1);
		
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		
		if(result.getTextCheckForPublication4().getText().contains("French") && result.getTextCheckForPublication4().getCssValue("background-color").contains("255")){
			
			System.out.println("Test #4 for search using Publication Type passed..");
		} else {
			
			System.out.println("Test #4 for search using Publication Type failed.");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictNameSearch1(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #1 for search using Name Type");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		
		jvr.getNameSearch().sendKeys("Paul");
		forceWait(1);
		jvr.getNameOptions().sendKeys(Keys.CONTROL);
		
		jvr.getDefendantAttorney().click();
		jvr.getDefendantExpert().click();
		jvr.getdefendantPhysician().click();
		jvr.getAllCategories().click();
		
		forceWait(2);
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		if (result.getContinueButton().isDisplayed()){
			
			result.getContinueButton().click();
		}
		
		forceWait(2);
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(1);
		
//		System.out.println(result.getTextCheckForName1().getCssValue("background-color"));
		if(result.getTextCheckForName1().getText().contains("Paul") && result.getTextCheckForName1().getCssValue("background-color").contains("255")){
			
			System.out.println("Test #1 for search using Name type passed...");
		} else {
			
			System.err.println("Test #1 for search using Name type failed.");
		}
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictNameSearch2(){
//		Ticket # 7649
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #2 for search using Name Type");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		
		jvr.getNameSearch().sendKeys("Gary Schaer");
		forceWait(1);
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		result.getSelectAllResults().click();
		result.getSelected().click();
		forceWait(2);
		
		if(result.getTextCheckForName2().getText().contains("Gary") && result.getTextCheckForName2().getCssValue("background-color").contains("255")){
			
			System.out.println("Test #2 for search using Name type passed...");
		} else {
			
			System.err.println("Test #2 for search using Name type failed.");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictNameSearch3(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #3 for search using Name Type");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		
		jvr.getNameSearch().sendKeys("Gary Schaer");
		forceWait(1);
		jvr.getDefendantExpert().click();
		jvr.getAllCategories().click();
		forceWait(1);
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(2);
		
		if(result.getTextCheckForName3().getText().contains("Gary") && result.getTextCheckForName3().getCssValue("backgroudn-color").contains("255")){
			
			System.out.println("Test #3 for search using name & category passed. ");
		} else {
			
			System.err.println("Test #3 for search using name & category failed..");
		}
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictNameSearch4(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #4 for search using Name Type");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		
		jvr.getNameSearch().sendKeys("Arnold B. Swerdlow");
		forceWait(1);
		jvr.getPlaintiffExpert().click();
		jvr.getAllCategories().click();
		forceWait(1);
		
		jvr.getJVRSearchButton().click();
		forceWait(1);
		
		result.getFirstResultCheckBox().click();
		result.getSelected().click();
		forceWait(1);
		
		if(result.getTextCheckForName4().getText().contains("Arnold B. Swerdlow") && result.getTextCheckForName4().getCssValue("background-color").contains("255")){
			
			System.out.println("Test #4 for search using name & category passed.");
		} else{
			
			System.err.println("Test #4 for search using name & category failed..");
		}
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictNameSearch5(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #5 for search using Name Type");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		
		jvr.getNameSearch().sendKeys("Phillips");
		forceWait(1);
		jvr.getPlaintiff().click();
		jvr.getAllCategories().click();
		
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		result.getSelectAllResults().click();
		result.getSelected().click();
		forceWait(2);
		
		if(getText("Phillip ") == true){
			
			System.err.println("Test #5 for search using Name type failed...");
			
		} else {
			
			System.out.println("Test #5 for search using Name type passed.");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictNameSearch6(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #6 for search using Name Type");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		
		jvr.getNameSearch().sendKeys("Jennifer A. Winter");
		forceWait(1);
		
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(1);
		
		if(result.getTextCheckForName6().getText().contains("Jennifer A. Winter") && result.getTextCheckForName6().getCssValue("background-color").contains("255")){
			
			System.out.println("Test #6 for search using Name type passed.");
		} else {
			
			System.err.println("Test #6 for search using Name type failed..");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void verdictNameSearch7(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #7 for search using Name Type");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		
		jvr.getNameSearch().sendKeys("Jennifer Winter");
		forceWait(1);
		
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(1);
		
if(result.getTextCheckForName6().getText().contains("Jennifer A. Winter") && result.getTextCheckForName6().getCssValue("background-color").contains("255")){
			
			System.out.println("Test #7 for search using Name type passed.");
		} else {
			
			System.err.println("Test #7 for search using Name type failed..");
		}

			main.getHomeIcon().click();
			forceWait(2);
	}
	
	@Test
	public void advancedSearch1(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #1 for search using Advanced Categories");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		forceWait(2);
		jvr.getAdvancedSearch().click();
		
		selectAdvancedCategory("Jurisdiction (IL)");
		selectRefinedCategory("Circuit Courts By County");
		jvr.getSelectAllLink().click();
		forceWait(3);
		
		selectRefinedCategory("Cook County");
		jvr.getSelectAllLink().click();
		forceWait(2);
		
		selectRefinedCategory("Federal");
		jvr.getSelectAllLink().click();
		forceWait(2);
		
		jvr.getSaveCategoriesButton().click();
		forceWait(1);
		
		jvr.getJVRSearchButton().click();
		forceWait(2);
		result.getContinueButton().click();
		if(result.getResultCount().getText().contains("200") && getText("/WI") != true ){
			
			System.out.println("Test #1 for Search using Advanced Categories passed.");
		} else {
			
			System.err.println("Test #1 for search using Advanced Categories failed..");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void advancedSearch2(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #2 for search using Advanced Categories");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		forceWait(2);
		jvr.getAdvancedSearch().click();
		
		selectAdvancedCategory("Jurisdiction (outside IL)");
		forceWait(1);
		selectRefinedCategory("Indiana");
		forceWait(1);
		jvr.getSelectAllLink().click();
		forceWait(2);
		
		selectRefinedCategory("Wisconsin");
		forceWait(1);
		jvr.getSelectAllLink().click();
		forceWait(2);
		jvr.getSaveCategoriesButton().click();
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		result.getContinueButton().click();
		if(result.getResultCount().getText().contains("200") && getText("/IL") != true){
			
			System.out.println("Test #2 for search using Advanced Categories passed.");
		} else {
			
			System.err.println("Test #2 for search using Advanced Categories failed....");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
		
	}
	
	@Test
	public void advancedSearch3(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #3 for search using Advanced Categories");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		forceWait(2);
		jvr.getAdvancedSearch().click();
		
		selectAdvancedCategory("Jurisdiction (outside IL)");
		forceWait(1);
		
		selectRefinedCategory("Wisconsin");
		forceWait(1);
		
		selectSpecificTerm("WI, Milwaukee 1st Jud Dist");
		forceWait(1);
		
		selectAdvancedCategory("Awards");
		forceWait(1);
		
		selectRefinedCategory("TOTAL MONETARY");
		forceWait(1);
		
		selectSpecificTerm("Over $50,000,000");
		
		jvr.getSaveCategoriesButton().click();
		forceWait(1);
		jvr.getJVRSearchButton().click();
		
		forceWait(1);
		
		if(getText("$ 63,335,819") == true && getText("$99,250,000") == true && getText("$104,450,000") == true ){
			
			System.out.println("Test #3 for search using Advanced Category passed.");
			
		} else {
			
			System.err.println("Test #3 for search using Advanced Category failed...");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void advancedSearch4(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #4 for search using Advanced Categories");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		forceWait(2);
		jvr.getAdvancedSearch().click();
		
		selectAdvancedCategory("Body Parts");
		forceWait(1);
		
		selectRefinedCategory("PELVIS/HIP");
		forceWait(1);
		jvr.getSelectAllLink().click();
		forceWait(1);
		
		jvr.getSaveCategoriesButton().click();
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		result.getContinueButton().click();
		forceWait(2);
		
		if(result.getResultCount().getText().contains("200")){
			
			System.out.println("Test #4 for search using Advanced Categories passed.");
		} else {
			
			System.err.println("Test #4 for search using Advanced Categories failed.");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void advancedSearch5(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #5 for search using Advanced Categories");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		forceWait(2);
		jvr.getAdvancedSearch().click();
		
		selectAdvancedCategory("Body Parts");
		forceWait(1);
		
		selectRefinedCategory("PELVIS/HIP");
		forceWait(1);
		
		selectSpecificTerm("Hip");
		jvr.getSaveCategoriesButton().click();
		forceWait(1);
		
		jvr.getJVRSearchButton().click();
		forceWait(2);
		result.getContinueButton().click();
		forceWait(1);
		
		if(result.getResultCount().getText().contains("200")){
			
			System.out.println("Test #5 for search using Advanced Categories passed.");
		} else {
			
			System.err.println("Test #5 for search using Advanced Categories failed..");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
		
		tearDown();
	}
	
	@Test
	public void inDepthTest1(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #1 for In Depth Testing");
		logIn();
		
		forceWait(2);
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		forceWait(2);
		
		jvr.getKeyword().sendKeys("Evans OR Diamond");
		
		jvr.getAdvancedSearch().click();
		forceWait(1);
		
		selectAdvancedCategory("Categories/Topics");
		forceWait(1);
		
		selectRefinedCategory("CIVIL RIGHTS");
		forceWait(1);
		
		selectSpecificTerm("Civil Rights");
		jvr.getSaveCategoriesButton().click();
		forceWait(1);
		
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		if(getText("Civil Rights") == true){
			
			System.out.println("Test #1 for In Depth Testing passed.");
		} else {
			
			System.err.println("Test #1 for In Depth Testing failed.....");
		}
		
		main.getHomeIcon().click();
		forceWait(2);
	}
	
	@Test
	public void inDepthTest2(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #2 for In Depth Testing");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		forceWait(2);
		
		jvr.getKeyword().sendKeys("bus");
		
		jvr.getStartDate().sendKeys("01/01/2011");
		jvr.getEndDate().clear();
		jvr.getEndDate().sendKeys("03/01/2011");
		
		jvr.getJVRSearchButton().click();
		forceWait(1);
		
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		
		if(result.getTextCheckForInDepth2().getText().contains("bus") && result.getTextCheckForInDepth2().getCssValue("background-color").contains("255")){
			
			System.out.println("Test #2 for Indepth Testing passed..");
		} else {
			
			System.err.println("Test #2 for Indepth testing failed.");
		}
		
		
	}
	
	@Test
	public void inDepthTest3(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #3 for In Depth Testing");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		forceWait(2);
		
		jvr.getNameSearch().sendKeys("smith");
		
		jvr.getAdvancedSearch().click();
		forceWait(1); 
		
		selectAdvancedCategory("Awards");
		forceWait(1);
		selectRefinedCategory("TOTAL MONETARY");
		forceWait(1);
		selectSpecificTerm("$9,999,999");
		forceWait(1);
		jvr.getSaveCategoriesButton().click();
		forceWait(2);
		
		jvr.getJVRSearchButton().click();
		forceWait(2);
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(1);
		if(result.getTextCheckForInDepth3().getText().contains("Smith") && result.getTextCheckForInDepth3().getCssValue("background-color").contains("255")){
			
			System.out.println("Test #3 for In Depth Testing passed..");
		} else {
			
			System.err.println("Test #3 for In Depth Testing failed.");
		}
		
		
	}
	
	@Test
	public void inDepthTest4(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #4 for In Depth Testing");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		forceWait(2);
		
		jvr.getKeyword().sendKeys("ladder");
		
		jvr.getAdvancedSearch().click();
		forceWait(1);
		
		selectAdvancedCategory("Categories/Topics");
		forceWait(1);
		selectRefinedCategory("PRODUCT LIABILITY");
		forceWait(1);
		jvr.getSelectAllLink().click();
		forceWait(1);
		jvr.getSaveCategoriesButton().click();
		forceWait(2);
		
		jvr.getJVRSearchButton().click();
		forceWait(2);
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(1);
		
		if(result.getTextCheckForInDepth4().getText().contains("LADDER") && result.getTextCheckForInDepth4().getCssValue("background-color").contains("255")){
			
			System.out.println("Test #4 for In Depth Testing passed..");
		} else {
			
			System.err.println("Test #4 for In Depth Testing failed.");
		}
	}
	
	@Test
	public void inDepthTest5(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #5 for In Depth Testing");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		forceWait(2);
		jvr.getAdvancedSearch().click();
		forceWait(1);
		
		selectAdvancedCategory("Demographics");
		forceWait(1);
		selectRefinedCategory("Gender");
		forceWait(1);
		selectSpecificTerm("Male");
		forceWait(1);
		
		selectRefinedCategory("Age Range");
		forceWait(1);
		selectSpecificTerm("49 YEARS");
		forceWait(1);
		
		selectAdvancedCategory("Body Parts");
		forceWait(1);
		selectRefinedCategory("ANKLE (TARSUS)");
		forceWait(1);
		jvr.getSelectAllLink().click();
		forceWait(1);
		
		selectRefinedCategory("FOOT");
		forceWait(1);
		jvr.getSelectAllLink().click();
		
		selectAdvancedCategory("Jurisdiction (IL)");
		forceWait(1);
		selectRefinedCategory("Circuit Courts By County");
		forceWait(1);
		selectSpecificTerm("IL, Will 12th Jud Cir");
		forceWait(1);
		
		jvr.getSaveCategoriesButton().click();
		forceWait(2);
		
		jvr.getJVRSearchButton().click();
		forceWait(2);
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(1);
		
		System.out.println(result.getTextCheckForInDepth5().getText());
		
		if(result.getTextCheckForInDepth5().getText().contains("(IL, Will 12th Jud Cir)") && getText("foot") == true && getText("ankle") == true){
			
			System.out.println("Test #5 for In Depth Testing passed..");
		} else {
			
			System.err.println("Test #5 for In Depth Testing failed.");
		}
		
	}
	
	@Test
	public void inDepth6(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #6 for In Depth Testing");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		forceWait(2);
		jvr.getAdvancedSearch().click();
		forceWait(1);
		
		selectAdvancedCategory("Demographics");
		forceWait(1);
		selectRefinedCategory("Gender");
		forceWait(1);
		selectSpecificTerm("Female");
		forceWait(1);
		
		selectRefinedCategory("Age Range");
		forceWait(1);
		selectSpecificTerm("29 YEARS");
		forceWait(1);
		
		selectAdvancedCategory("Categories/Topics");
		forceWait(1);
		selectRefinedCategory("ANIMAL INJURIES");
		forceWait(1);
		jvr.getSelectAllLink().click();
		forceWait(1);
		
		selectRefinedCategory("CIVIL RIGHTS");
		forceWait(1);
		jvr.getSelectAllLink().click();
		
		selectRefinedCategory("PRODUCT LIABILITY");
		forceWait(1);
		jvr.getSelectAllLink().click();
		
		jvr.getSaveCategoriesButton().click();
		forceWait(2);
		
		jvr.getJVRSearchButton().click();
		forceWait(2);
		result.getFirstResultForKeyword().click();
		result.getSelected().click();
		forceWait(1);
		
		if(getText("dog") == true){
			
			System.out.println("Test #6 for In Depth Testing passed..");
		} else {
			
			System.err.println("Test #6 for In Depth Testing failed.");
		}
		
	}
	
	@Test
	public void inDepth7(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #7 for In Depth Testing");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		forceWait(2);
		jvr.getAdvancedSearch().click();
		forceWait(1);
		
		selectAdvancedCategory("Demographics");
		forceWait(1);
		selectRefinedCategory("Age Range");
		forceWait(1);
		selectSpecificTerm("FETUS");
		forceWait(1);
		selectSpecificTerm("NEWBORN");
		forceWait(1);
		selectSpecificTerm("INFANT");
		forceWait(1);
		
		selectAdvancedCategory("Categories/Topics");
		forceWait(1);
		selectRefinedCategory("ANIMAL INJURIES");
		forceWait(1);
		jvr.getSelectAllLink().click();
		
		selectRefinedCategory("PRODUCT LIABILITY");
		forceWait(1);
		jvr.getSelectAllLink().click();
		
		selectAdvancedCategory("Jurisdiction (IL)");
		forceWait(1);
		selectRefinedCategory("Cook County");
		forceWait(1);
		jvr.getSelectAllLink().click();
		
		selectRefinedCategory("Circuit Courts By County");
		forceWait(1);
		selectSpecificTerm("IL, Kane 16th Jud Cir");
		forceWait(1);
		
		jvr.getSaveCategoriesButton().click();
		forceWait(2);
		
		jvr.getJVRSearchButton().click();
		forceWait(2);
		result.getSelectAllResults().click();
		result.getSelected().click();
		forceWait(1);
		
		
		if(getText("(IL Cook-Law)") && getText("fetus") && getText("dog")){
			
			System.out.println("Test #7 for In Depth Testing passed..");
		} else {
			
			System.err.println("Test #7 for In Depth Testing failed.");
		}
		
	}
	
	@Test
	public void inDepth8(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #8 for In Depth Testing");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		forceWait(2);
		
		jvr.getKeyword().sendKeys("car");
		forceWait(1);
		
		jvr.getAdvancedSearch().click();
		forceWait(1);
		selectAdvancedCategory("Awards");
		forceWait(1);
		selectRefinedCategory("PLAINTIFF MONETARY");
		forceWait(1);
		selectSpecificTerm("$4,999,999");
		forceWait(1);
		
		jvr.getSaveCategoriesButton().click();
		forceWait(2);
		
		jvr.getJVRSearchButton().click();
		forceWait(2);
		if (result.getContinueButton().isDisplayed()){
			
			result.getContinueButton().click();
		}
		forceWait(1);
		
		result.getSelectAllResults().click();
		result.getSelected().click();
		forceWait(1);
		
		if(getText("car") && getText("CAR")){
			
			System.out.println("Test #8 for In Depth Testing passed..");
		} else {
			
			System.err.println("Test #8 for In Depth Testing failed.");
		}
		
		
	}
	
	@Test
	public void inDepth9(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #9 for In Depth Testing");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		forceWait(2);
		
		jvr.getAdvancedSearch().click();
		forceWait(1);
		selectAdvancedCategory("Awards");
		forceWait(1);
		selectRefinedCategory("NON MONETARY");
		forceWait(1);
		selectSpecificTerm("(T) Deadlock");
		forceWait(1);
		
		jvr.getSaveCategoriesButton().click();
		forceWait(2);
		
		jvr.getJVRSearchButton().click();
		forceWait(2);
		if (result.getContinueButton().isDisplayed()){
			
			result.getContinueButton().click();
		}
		forceWait(1);
		
		result.getSelectAllResults().click();
		result.getSelected().click();
		forceWait(2);
		
		if(getText("Deadlock")){
			
			System.out.println("Test #9 for In Depth Testing passed..");
		} else {
			
			System.err.println("Test #9 for In Depth Testing failed.");
		}
	}
	
	@Test
	public void inDepth10(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #10 for In Depth Testing");
//		logIn();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		
		lr.getJVS().click();
		forceWait(2);
		jvr.getReset().click();
		forceWait(2);
		
		jvr.getAdvancedSearch().click();
		forceWait(1);
		selectAdvancedCategory("Awards");
		forceWait(1);
		selectRefinedCategory("NON MONETARY");
		forceWait(1);
		selectSpecificTerm("(T) Mistrial");
		forceWait(1);
		
		jvr.getSaveCategoriesButton().click();
		forceWait(2);
		
		jvr.getJVRSearchButton().click();
		forceWait(2);
		
		result.getSelectAllResults().click();
		result.getSelected().click();
		forceWait(2);
		
		if(getText("Mistrial")){
			
			System.out.println("Test #10 for In Depth Testing passed..");
		} else {
			
			System.err.println("Test #10 for In Depth Testing failed.");
		}
	}
	
	@Test
	public void multiStepTest1(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #1 for Multi-Step Testing");
//		logIn();
		
		goToURL("https://www.lawyerport.com/attorneys#/1004059/contact-info");
		forceWait(1);
		result.getMoreButtonForJVRForPhilipCorboy().click();
		forceWait(2);
		
		if(result.getResultCount().getText().contains("79")){
			
			System.out.println("Total results matches.. ");
			
			result.getFirstResultCheckBox().click();
			result.getSelected().click();
			forceWait(2);
			if(getText("Philip")){
				
				result.getSearchResults().click();
				forceWait(2);
				if(result.getResultCount().getText().contains("79")){
					
					System.out.println("Test #1 for Multi-Step Testing passed...");
				}
				
				
			} else {
				
				System.err.println("Test #1 for Multi-Step Testing failed.");
			}
		} else {
			
			System.err.println("Total results doesn't match");
		}
		
		
	}
	
	
	
	@Test
	public void multiStepTest2(){
		
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Test #2 for Multi-Step Testing");
//		logIn();
		
		forceWait(1);
		goToURL("https://www.lawyerport.com/attorneys#/1004059/contact-info");
		forceWait(1);
		
		result.getMoreButtonForJVRForPhilipCorboy().click();
		forceWait(2);
		result.getFirstResultCheckBox().click();
		result.getSelected().click();
		forceWait(2);
		
		goBackUsingBrowserBack();
		
		scrollDown();
		
		if(result.getResultCount().getText().contains("79")){
			
			System.out.println("Test #2 for Multi-Step Testing passed..");
		} else {
			
			System.err.println("Test #2 for Multi-Step Testing failed.");
		}
		
		
		
	}
	
	
	public VerifyJuryVerdicts selectAdvancedCategory(String categoryName){
		List<WebElement> groups = jvr.getAdvancedCategoryList();
		for(WebElement group : groups){
			
			if(group.getText().equals(categoryName)){
				
				group.click();
				break;
			}
		}
		return this;
	}
	
	public VerifyJuryVerdicts selectRefinedCategory(String refineCategoryName){
		
		List<WebElement> groups = jvr.getRefineCategoryList();
		for(WebElement group : groups){
			
			if(group.getText().equals(refineCategoryName)){
				
				group.click();
				break;
			}
		}
		return this;
	}
	
	public VerifyJuryVerdicts selectSpecificTerm(String specificCategoryName){
		
		List<WebElement> groups = jvr.getSpecificTermList();
		for(WebElement group : groups){
			
			if(group.getText().contains(specificCategoryName)){
				
				group.click();
				break;
			}
		}
		return this;
	}
	
	
}
