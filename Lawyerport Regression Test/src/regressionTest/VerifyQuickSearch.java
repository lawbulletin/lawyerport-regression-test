package regressionTest;

import org.junit.*;
import org.openqa.selenium.WebDriver;

import lp_dominion.AbstractSprint;
import lp_dominion.PFCaller;
import pagefactorylibrary.DirectoryResultFactory;
import pagefactorylibrary.LPMainPageFactory;
import lp_dominion.ReuseableMethod;

public class VerifyQuickSearch extends AbstractSprint{
	
	LPMainPageFactory main = PFCaller.lpMainFactory();
	DirectoryResultFactory dir = PFCaller.dirResultsFactory();
	ReuseableMethod use = new ReuseableMethod();	
	
	
//	@Test
//	public void quickSearchTest() throws Exception{
//		
//		System.out.println("Verify Quick Search for People");
//		logIn();
//		main.getSearchDropdown().click();
//		main.getSearchDropdownSelectionPeople().click();
//		main.getSearchTextbox().sendKeys(randomLastName());
//		
//		forceWait(1);
//		main.getSearchButton().click();
//		forceWait(1);
//		
//		String results = dir.getResultCount().getText().toString();
//		System.out.println("QuickSearch Results :");
//		System.out.println(results);
//		condition1 = results.contains("People");
//		tfTest();
//	}
	
	@Test
	public void lawFirmFilterTest() throws Exception{
		
		//logIn();
		System.out.println("Verify Quick Search for People");
		main.getSearchDropdown().click();
		main.getSearchDropdownSelectionPeople().click();
		main.getSearchTextbox().sendKeys(randomLastName());
		forceWait(1);
		main.getSearchButton().click();
		forceWait(1);
		
		String results = dir.getResultCount().getText().toString();
		System.out.println("QuickSearch Results :");
		System.out.println(results);
		condition1 = results.contains("People");
		System.out.println("Set Law Firm Filter");
		dir.getLawFirmFilter().click();
		String URL = getURL();
		if(URL.contains("orgType=1_lawfirm")){
			System.out.println("Law Firm filter is valid.");
		}
		
		forceWait(1);
		 String results2 = dir.getResultCount().getText().toString();
		 System.out.println("Filter Results:");
		 System.out.println(results2);
		 
		 //tearDown();
	}
	
	

}
