package regressionTest;

import lp_dominion.PFCaller;
import pagefactorylibrary.HomePageFactory;
import pagefactorylibrary.LPMainPageFactory;
import pagefactorylibrary.LoginPageFactory;
import seleniumWebDriver.SeleniumDriver;

import org.junit.Test;

import lp_dominion.AbstractSprint;

public class VerifyLogout extends AbstractSprint{
	
	@Test
	public void logout() throws Exception{
		
		SeleniumDriver.getDriver().quit();
		//tearDown();
	}
}
