package regressionTest;

import lp_dominion.AbstractSprint;
import lp_dominion.PFCaller;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import pagefactorylibrary.LPMainPageFactory;
import seleniumWebDriver.SeleniumDriver;



public class VerifyLogin extends AbstractSprint{
	
	LPMainPageFactory home = PFCaller.lpMainFactory();
	
	
	@Test
	public void test1VerifyProperLogin() throws Exception{
		System.out.println("Verify Login to main page with prooper credentials");
		logIn();
		String welcome = home.getWelcomeName().getText().toString().toLowerCase();
		
		condition1 = welcome.contains("welcome");
		tfTest();
		//tearDown();
		
	}
	


}
