package pagefactorylibrary;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


/**
 *  Page Factory Library to hold Court Docket element locator information 
 * @author hipatel
 *
 */
public class CDPageFactory {
	
	@FindBy (how = How.XPATH, using = "//li[@id='navRes']")
	private WebElement getDocket; 
	
	@FindBy(how = How.ID, using ="form-field-0")
	private WebElement caseNum;
	
	@FindBy(how = How.CSS, using ="input[placeholder='Select from type-ahead list']")
	private WebElement judge; 
	
	@FindBy(how = How.CSS, using = "li[id='yui_3_4_1_184_1448988932571_8689']")
	private WebElement judgeList;

	@FindBy(how = How.ID, using ="form-field-2")
	private WebElement plaintlff; 
	
	@FindBy(how = How.ID, using ="form-field-3")
	private WebElement defendent; 
	
	@FindBy(how = How.CSS, using ="div[class='form-field-wrapper court-dockets-lawyerFirm'] div div li input")
	private WebElement lawyerFirm; 
	
	@FindBy(how = How.ID, using ="form-field-5")
	private WebElement plaintiffDefendant; 
		
	@FindBy(how = How.ID, using ="form-field-7")
	private WebElement startDate; 
	
	@FindBy(how = How.ID, using ="form-field-8")
	private WebElement endDate; 
	
	@FindBy(how = How.ID, using ="form-field-9")
	private WebElement jursidiction; 
	
	@FindBy(how = How.ID, using ="court-dockets-run-advanced-search")
	private WebElement search; 
	
	@FindBy(id ="court-dockets-reset-advanced-search-bottom")
	private WebElement reset; 
	
	
//	/**
//	 * nav to court docket page
//	 */
//	public WebElement navCourtDocket() {
//		return getDocket;
//	
//	}
	
	public WebElement getJudgeList(){
		
		return judgeList;
	}
	
	public WebElement getDocketSearchButton() {
		return search;
	}
	public WebElement getDocketRestSearchButton() {
		return reset;
	}
	
	public WebElement getDocketCaseNum() {
		return caseNum;
	}
	
	public WebElement getDocketJudge() {
		return judge;
	}
	
	public WebElement getDocketPlaintiff() {
		return plaintlff;
	}
	public WebElement geDocketDefendent() {
		return defendent;
	}
	public WebElement getDocketLawyerFirm() {
		return lawyerFirm;
	}
	public WebElement getDocketPlaintiffDefendant() {
		return plaintiffDefendant;
	}
	public WebElement getDocketStartDate() {
		return startDate;
	}
	public WebElement getDocketEndDate() {
		return endDate;
	}
}
