package pagefactorylibrary;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CaseDetailsPageFactory {
	
	@FindBy(id="case-details-modal-caseName")
		private WebElement caseName;
	
	@FindBy(id="case-details-modal-caseNumber")
		private WebElement caseNumber;
	
	@FindBy(id="case-details-modal-activity")
		private WebElement activity;
	
	@FindBy(id="case-details-modal-attorneyName")
		private WebElement attorneyName;
	
	@FindBy(id="case-details-modal-activityDate")
		private WebElement activityDate;
	
	@FindBy(id="case-details-modal-attorneyAddress")
		private WebElement attoneyAddress;
		
	@FindBy(id="case-details-modal-attorneyCityState")
		private WebElement attorneyCityState;
	
	@FindBy(id="case-details-modal-attorneyZip")
		private WebElement attorneyZip;
	
	@FindBy(id="case-details-modal-fees")
		private WebElement fees;
	
	@FindBy(id="modal-buttons-container")
		private WebElement container;
	
	@FindBy(id="case-details-modal-party")
		private WebElement party;
	
	public WebElement getParty(){
		return party;
	}
	
	public WebElement getCaseName(){
		return caseName;
	}
	
	public WebElement getCaseNumber(){
		return caseNumber;
	}
	
	public WebElement getActivity(){
		return activity;
	}
	
	public WebElement getAttorneyName(){
		return attorneyName;
	}
	
	public WebElement getActivityDate(){
		return activityDate;
	}
	
	public WebElement getAttorneyAddress(){
		return attoneyAddress;
	}
	
	public WebElement getAttorneyState(){
		return attorneyCityState;
	}
	
	public WebElement getAttorneyZip(){
		return attorneyZip;
	}
	
	public WebElement getFees(){
		return fees;
	}
	
	public WebElement getContainer(){
		return container;
	}
	

}
