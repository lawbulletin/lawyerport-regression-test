package pagefactorylibrary;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

//https://weblogs.java.net/blog/johnsmart/archive/2010/08/09/selenium-2web-driver-land-where-page-objects-are-king
//http://mestachs.wordpress.com/2012/08/13/selenium-best-practices/
public class DirectoryPageFactory{
	
	@FindBy(how = How.ID, using ="edit-profile")
	private WebElement editButton;
	
	@FindBy (how = How.XPATH, using = "//li[@id='navDir']")
	private WebElement getDirctory; 

	@FindBy(how = How.ID, using = "form-field-0")
	private WebElement firstName;
	
	@FindBy(how = How.ID, using = "form-field-1")
	private WebElement primaryOrg;
	
	@FindBy(how = How.ID, using = "form-field-2")
	private WebElement middleName;
	
	@FindBy(how = How.ID, using = "form-field-3")
	private WebElement peopleTitle;
	
	@FindBy(how = How.ID, using = "form-field-4")
	private WebElement lastName;
	
	@FindBy(how = How.ID, using = "form-field-5")
	private WebElement peopleOrgCity;
	
	@FindBy(how = How.ID, using = "form-field-6")
	private WebElement FormerlyKnown;
	
	@FindBy(how = How.ID, using = "form-field-9")
	private WebElement selectState;
	
	@FindBy(how = How.ID, using = "dir-run-advanced-search")
	private WebElement search;
	
	@FindBy (how = How.ID, using = "btnPracticeArea")
	private WebElement practiceArea; 
	
	@FindBy(id = "subtab-people")
	private WebElement peopleButton;
	
	@FindBy(id = "subtab-org")
	private WebElement orgButton;
	
	@FindBy( id="dir-reset-advanced-search-bottom")
	private WebElement resetButton;
	
	public WebElement getReset(){
		return resetButton;
	}
	
	public WebElement getPeopleButton() {
		return peopleButton;
	}
	
	public WebElement getOrgButton() {
		return orgButton;
	}
	
	public WebElement getEditButton() {
		return editButton;
	}
	
	public WebElement navDIR() {
		return getDirctory;
	}
	
	public WebElement getFirstName() {
		return firstName;
	}
	public WebElement getPeopleOrg() {
		return primaryOrg; 
	}
	public WebElement getMiddleName() {
		return middleName; 
	}
	public WebElement getLastName() {
		return lastName;
	}
	public WebElement getTitle() {
		return peopleTitle;
	}
	
	public WebElement getPeopleOrgCity() {
		return peopleOrgCity; 
	}
	public WebElement getFormelyknownAs() {
		return FormerlyKnown;
	}
	
	public WebElement getSearchButton() {
		return search;		
	}


}




