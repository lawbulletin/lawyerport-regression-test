package pagefactorylibrary;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class OrgProfilePageFactory {

	@FindBy(how = How.CLASS_NAME, using = "profile-title")
	private WebElement orgTitle;
	
	public WebElement getOrgTitle(){
		return orgTitle;
	}
}
