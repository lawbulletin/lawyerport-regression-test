package pagefactorylibrary;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LegalMarketPlacePageFactory {
	
	@FindBy(how= How.ID, using = "navVend")
	private WebElement legalMarketPlace;
	
	@FindBy(how = How.ID, using = "form-field-0")
	private WebElement compnayName;
	
	@FindBy(how = How.ID, using = "marketplace-search-run-advanced-search")
	private WebElement searchButton;

	
	public WebElement navLegalMerketPlace() {
		return legalMarketPlace; 
	}
	
	public WebElement getCompnayName() {
		return compnayName;
	}
	
	public WebElement getSearchButton() {
		return searchButton; 
	}
	

}
